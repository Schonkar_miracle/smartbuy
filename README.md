###  TO-DO
- Style the front end
- User ID or Username? Add those to session vars.
- figure out the User authentication.




_dependant tasks_
- add the update flow func.
  - SAP - x | API AI - yes
- add func to get orders for particular user.

_DONE_
- [not required] Replace long JSON references like `response.result.parameters.number` with lodash.pick references.
- handle the create order flow
- [not erquired] In create order, write logic to add order to respective Users and Manager arrays once the order is created.
