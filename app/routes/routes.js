module.exports = function(app, router){

  var orderController = require ('../controllers/order.controller.js')
  var userController = require ('../controllers/user.controller.js')

  app.route('/users')// Get all the available users
  .get(userController.list)
  app.route('/users/:user_id')
  .get(userController.read)
  app.route('/users/:user_id/placed')
  .get(userController.getAllPlacedOrders)
  app.route('/users/:user_id/pending')
  .get(userController.getAllPendingOrders)
  app.route('/users/:user_id/served')
  .get(userController.getAllServedOrders)


  app.route('/orders')
  .get(orderController.list)// Get all the available orders
  .post(orderController.create);

  app.route("/orders/:order_id")
  .get(orderController.read)// get order by its ID
  .put(orderController.update);// update order of that ID

  app.route("/test")
  .post(orderController.test);

  // route middleware to validate :user_id
  app.param('user_id', userController.userById);
  app.param('order_id', orderController.orderById);


};
