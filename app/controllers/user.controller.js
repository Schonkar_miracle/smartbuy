var mongoose = require('mongoose'),
errorHandler = require('./errors.controller'),
Order = mongoose.model('Order'),
User = mongoose.model('User');


//get all placed orders for the user
exports.getAllPlacedOrders = function(req, res){
  return res.json(req.user.placedOrders);
};

//get all pending approval orders for the user
exports.getAllPendingOrders = function(req, res){
  return res.json(req.user.pendingOrders);
};

//get all served orders for the user
exports.getAllServedOrders = function(req, res){
  return res.json(req.user.servedOrders);
};

// User routes
exports.list = function (req, res) {
  User.find().exec(function (err, users) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(users);
    }
  })
};

exports.read = function(req, res){
  res.json(req.user);
}

//userID middleware
exports.userById = function (req, res, next, user_id) {
  console.log('Middleware executes');
  //check if user exists
  User.findById(user_id).exec(function (err, user) {
    if (err) return next(err);
    if (!user) {
      return res.status(400).send({
        message: "User not found"
      })
    } else {
      req.user = user;
      next();
    }
    // res.json(user);
  })
};
