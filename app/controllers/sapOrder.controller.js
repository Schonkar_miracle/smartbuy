var mongoose = require('mongoose'),
errorHandler = require('./errors.controller'),
request = require('request'),
unirest = require('unirest'),
_ = require('lodash');
var session = require('express-session');
var chalk = require('chalk');


exports.getOrderByPo = function(poNumber){
  var reqUrl = "http://192.168.1.214:8011/sap/opu/odata/sap/ZSAP_GET_PODETAILS_SRV/GetPODetailsSet('" + poNumber + "')?$format=json";
  var bodyGlobal = {};
  var payload = {
    url: reqUrl,
    headers: {
      'Authorization': 'Basic cGl1c2VyOm1pcmFjbGU5'
    }
  };
  console.log("Requesting URL: " + payload.url + " | with headers: " + JSON.stringify(payload.headers));

  //added this get request to promise
  return new Promise(function(fulfill, reject){
    request.get(payload, function (err, response, body) {
      if (!err && response.statusCode == 200) {
        fulfill(body);
      } else {
        reject(err);
      }
    });
  });
};

exports.create = function(parameters, req){
  console.log(chalk.blue("resquest.session"));
  console.log(req.session);
  //create params appropriate for SAP POST req.
  var postData = {
    CompCode: "1000",
    DocType: "NB",
    ItemIntvl: "00010",
    Pmtterms: "ZB01",
    PurchOrg: "1000",
    PurchGrp: "100",
    Vendor: "1000",
    Item: {
      Material: "100-200",
      Plant: "1000",
      StgeLoc: "1038",
      Quantity: "10.000",
      NetPrice: "141.7000",
      DeliveryDate: "20170728"
    }
  };

  var getReqPayload = {
    url: "http://192.168.1.214:8011/sap/opu/odata/sap/ZSAP_PO_CREATE_SRV/",
    headers: {
      'Authorization': 'Basic cGl1c2VyOm1pcmFjbGU5',
      'X-CSRF-Token': 'fetch',
    }
  };

  return new Promise(function(fulfill, reject){
    //request to get the x-csrf token
    request.get(getReqPayload, function (err, response, body) {
      var token = response.headers["x-csrf-token"];
      var urlCreate = "http://192.168.1.214:8011/sap/opu/odata/sap/ZSAP_PO_CREATE_SRV/POCreateSet";
      console.log(chalk.blue("TOKEN IS: "+token));
      var postReqPayload = {
        url: urlCreate,
        headers: {
          'cache-control': 'no-cache',
          authorization: 'Basic cGl1c2VyOm1pcmFjbGU5',
          'content-type': 'application/json',
          accept: 'application/json',
          'x-csrf-token': token,
          cookie: response.headers["set-cookie"],
        },
        body:postData,
        json: true
      };
      //post to create the order in SAP.
      request.post(postReqPayload, function (err, response, body) {
        console.log(chalk.cyan("Payload to be sent"));
        console.log(postReqPayload);
        console.log(chalk.blue("Status Code: " + response.statusCode));
        console.log(chalk.yellow(JSON.stringify(body)));
        if (!err && response.statusCode == 201) {
          fulfill(body);
        } else {
          reject(err);
        }
      });
    });
    // all requests over.
  });

}
