var mongoose = require('mongoose'),
errorHandler = require('./errors.controller'),
request = require('request'),


Order = mongoose.model('Order'),
User = mongoose.model('User'),
_ = require('lodash');

//lists all orders - Deprecated when SAP is introduced.
exports.list = function (req, res) {
  Order.find().exec(function (err, orders) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(orders);
    }
  })
};

//creates new order  CREATE FUNCTIONALITY FOR MONGODB
/*exports.create = function (req, res) {
console.log(req.body.placedBy);
var order = new Order(req.body);
var orderId, userId = req.body.placedBy, managerId;
order.save(function (err) {
if (err) {
return res.status(400).send({
message: errorHandler.getErrorMessage(err)
});
}
else {
console.log(order);
orderId = order._id;
User.findByIdAndUpdate( // Updating the order ID in user's placedOrders.
userId,
{$push: {placedOrders: orderId}},
{safe: true, upsert: true},
function (err, userDetails) {
if (err) {
//Error handling
} else {
/!*console.log(err);
console.log("above this ERR..below this USER DOC");
console.log(userDetails);*!/
managerId = userDetails.managerId;
User.findByIdAndUpdate( // Updating the order ID in managers's pendingOrders.
managerId,
{$push: {pendingOrders: orderId}},
{safe: true, upsert: true},
function (err, managerDetails) {
if (err) {
//Error handling
} else {
console.log(managerDetails);
}
}
);
}
}
);
res.status(201).json(order)
}
});

};*/

exports.create = function (req, res) {
  console.log("ENTERED POST TO POST PO");
  var reqUrl = "http://192.168.1.214:8011/sap/opu/odata/sap/ZSAP_PO_CREATE_SRV/";
  var payload = {
    url: reqUrl,
    headers: {
      'Authorization': 'Basic cGl1c2VyOm1pcmFjbGU5',
      'X-CSRF-Token': 'fetch',
      'Accept':'application/json',
      'Content-Type':'application/json'
    },
    auth: {
      'user': 'piuser',
      'pass': 'miracle9',
      'sendImmediately': true
    }
  };

  request.get(payload, function (err, response, body) {
    /*console.log(body);
    console.log("body-----------");
    console.log(response);*/
    var token = response.headers["x-csrf-token"];
    var urlCreate = "http://192.168.1.214:8011/sap/opu/odata/sap/ZSAP_PO_CREATE_SRV/POCreateSet";
    var payloadCreate = {
      url: urlCreate,
      headers: {
        'Authorization': 'Basic cGl1c2VyOm1pcmFjbGU5',
        'X-CSRF-Token': token
      },
      auth: {
        'user': 'piuser',
        'pass': 'miracle9',
        'sendImmediately': false
      },
      data: req.body
    };
    // res.json(payloadCreate);
    request.post(payloadCreate, function (err, response, body) {
      // console.log("======================================================================");
      console.log("TOKEN==="+token);
      // console.log(response);
      console.log(body);
      res.json(body);

    })


  })
};

//order middleware
exports.orderById = function (req, res, next, order_id) {
  console.log("order Middleware");
  var reqUrl = "http://192.168.1.214:8011/sap/opu/odata/sap/ZSAP_GET_PODETAILS_SRV/GetPODetailsSet('" + order_id + "')?$format=json";

  var payload = {
    url: reqUrl,
    headers: {
      'Authorization': 'Basic cGl1c2VyOm1pcmFjbGU5'
    }
  };
  console.log("Requesting URL: " + payload.url + " | with headers: " + JSON.stringify(payload.headers));

  request.get(payload, function (err, response, body) {
    if (!err && response.statusCode == 200) {
      req.order = body;
    } else {
      console.log("Status: " + response.statusCode);
    }
    next();
    // res.json(order);
  });
}
//get single order by ID
exports.read = function (req, res) {
  res.json(JSON.parse(req.order));
}

exports.update = function (req, res) {
  var order = req.order; //get older order entry
  order = _.extend(order, req.body); //update changes

  order.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(order);
    }
  });
};


exports.test = function (req, res) {
  //test funtion
};
