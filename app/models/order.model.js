var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var OrderSchema = new Schema({
	createdOn:{
		type: Date,
		default: Date.now
	},
	placedBy:{
		type: String,
		default: "",
		required: 'Username cannot be blank'
	},
	orderedItemName:{
		type: String,
		default: "",
		required: 'Product cannot be blank'
	},
	orderQuantity:{
		type: Number,
		required: 'Quantity cannot be zero'
	},
	pricePerItem:{
		type: Number,
		default: "0.00"
	},
	deliveryAddress:{
		type: String,
		default: "",
		required: 'Username cannot be blank'
	},
	status:{
		type: String,
		default: "",
		required: 'status cannot be blank'
	},
	comment:{
		type: String,
		default: ""
	},
	servedBy:{
		type: String,
		default: ""
	},
	servedDate:{
		type: Date,
		default: ""
	}

	// managerAssigned:{
	// 	type: Schema.Types.ObjectId,
	// 	ref: 'Manager',
	// 	required: 'Manager required to process the order'
	// }
});

mongoose.model('Order', OrderSchema);
