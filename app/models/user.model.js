var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var userSchema = new Schema({
    name:{
        type: String,
        default: "",
        required: 'name cannot be blank'
    },
    department:{
        type: String,
        default: "",
        required: 'department cannot be blank'
    },
    managerId:{
        type: String,
        default: "",
        required: 'managerId cannot be blank'
    },
    role:{
        type: String,
        required: 'role cannot be blank'
    },
    placedOrders:{
        type: Array,
        default: []
    },
    pendingOrders:{
        type: Array,
        default: []
    },
    servedOrders:{
        type: Array,
        default: []
    }

    // managerAssigned:{
    // 	type: Schema.Types.ObjectId,
    // 	ref: 'Manager',
    // 	required: 'Manager required to process the order'
    // }
});

mongoose.model('User', userSchema);
