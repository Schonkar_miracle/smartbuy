var express = require('express');
var mongoose = require('mongoose');
var chalk = require('chalk');
var ai = require('apiai');
var appApi = ai('195f64affc824e5582065b8246120e28');
var bodyParser = require('body-parser');
var app = express();
var router = express.Router();
var session = require('express-session');


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/'));
app.use(session({secret: "secret"}));

var request = require('request');

//models
var Order = require('./app/models/order.model');
var User = require('./app/models/user.model');

//routes
require('./app/routes/routes')(app, router);

//controllers
var orderController = require ('./app/controllers/order.controller.js')
var sapOrderController = require ('./app/controllers/sapOrder.controller.js')


var port = process.env.PORT || 8080;

app.listen(port);
console.log("Server started at: " + port);

//DB connection
var hostname = '192.168.2.20';
// var hostname = 'localhost';
mongoose.connect('mongodb://'+ hostname +':27017/smartBuy').then(
  () => { console.log('connected to mongoDB at '+ hostname +':27017') },
  err => { console.log('failed connection') }
);


app.get('/',function(req, res){
  res.sendfile('./index.html')
});

app.use('/chat/api', function(req, res, next){
  console.log('/chat/api middleware hits');
  /* Middleware for the chat api.
  - Add user auth here.
  - Add user role recogniser
  */
req.session.user = {username: "schonkar", firstname: "Shrikar", lastname: "Chonkar"}
  next();
})


app.get('/chat/api/:text', function (req, res) {
  console.log("\n/chat/api/:text hits");
  console.log(chalk.green("\n====================== \n"));
  var request = appApi.textRequest(req.params.text, {
    sessionId: 'abc'
  });

  request.on('response', function (response) {
    console.log(response);
    console.log(chalk.green("\n === Reponse to front end Ends === \n"));
    //smallJSON of collected data.
    // var collectedData = _.pick(response.data.result,
    //   ['action', 'actionIncomplete', 'parameters', 'fulfillment.speech']);
    // }
    //handle the type of request //4500019070
    //CASE - GET ORDER STATUS
    var poNumber = response.result.parameters.number;
    console.log(poNumber);
    if(response.result.action == "Po-status"){
      //asks for status- get this response, modify it and give it back
      if(poNumber){ //check if number is present
        console.log("number entered works");
        sapOrderController.getOrderByPo(poNumber)
        .then(function(orderDetailsResponse){
          //If order is found
          response.orderDetails = JSON.parse(orderDetailsResponse);
          //modify the speech here.
          console.log(response.orderDetails);
          response.result.fulfillment.speech = "Order status for PO number " + response.orderDetails.d.Purchaseorder + " is: "
          return res.json(response);
        }).catch(function(orderDetailsError){
          //IF order is not found, do this
          response.result.fulfillment.speech = "Order number: "+ poNumber + " not found. Please make sure you entered the correct number"
          console.log(orderDetailsError);
          return res.json(response);
        });
      }else{
        return res.json(response);
      }
    }
    //CASE - PLACE ORDER
    else if(response.result.action == 'order-details'){
      if(!response.result.actionIncomplete){
        //Send post req now.
        var parameters = response.result.parameters;
        sapOrderController.create(parameters, req)
        .then(function(createdOrderResponse){ // get Reponse from the SAP Odata request.
          response.result.fulfillment.speech = response.result.fulfillment.speech + ". Your PO number is: " + createdOrderResponse.d.PoNum
          return res.json(response);
        }).catch(function(createdOrderError){
          console.log(createdOrderError);
        });
      }else{
        return res.json(response);
      }
    }

    //CASE - UPDATE ORDER DETAILS
    else if(response.result.action == 'change'){
      //update order
      return res.json(response);
    }else{
      console.log(response.result.fulfillment.speech);
      // return response.result.fulfillment.speech;
      return res.json(response);
    };
  });

  request.on('error', function (error) {
    console.log(error);
  });

  request.end();

});

app.get('/api/:message', function (req, res) {
  console.log(req.params.message);
});


exports = module.exports = app;
